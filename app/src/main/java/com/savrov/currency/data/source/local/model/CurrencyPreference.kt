package com.savrov.currency.data.source.local.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.savrov.currency.data.repo.model.CurrencyWithPreference

@Entity(tableName = "currency_preferences")
data class CurrencyPreference(@PrimaryKey @NonNull @ColumnInfo(name = "currency_code") val currencyCode: String,
                              @ColumnInfo(name = "is_base") var isBase: Boolean? = false,
                              @ColumnInfo(name = "is_favorite") var isFavorite: Boolean? = false) {

    companion object {

        fun valueOf(currency: CurrencyWithPreference): CurrencyPreference {
            return CurrencyPreference(currencyCode = currency.code, isBase = currency.isBase, isFavorite = currency.isFavorite)
        }

        fun valueOf(currency: CurrencyData): CurrencyPreference {
            return CurrencyPreference(currencyCode = currency.code)
        }

    }

}