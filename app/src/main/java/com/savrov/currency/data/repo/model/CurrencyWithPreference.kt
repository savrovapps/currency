package com.savrov.currency.data.repo.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo

data class CurrencyWithPreference(@ColumnInfo(name = "code") @NonNull val code: String,
                                  @ColumnInfo(name = "label") val label: String? = null,
                                  @ColumnInfo(name = "rate") val rate: Double? = null,
                                  @ColumnInfo(name = "is_base") var isBase: Boolean = false,
                                  @ColumnInfo(name = "is_favorite") var isFavorite: Boolean = false)