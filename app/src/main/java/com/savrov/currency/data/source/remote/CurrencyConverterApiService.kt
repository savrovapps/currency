package com.savrov.currency.data.source.remote

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyConverterApiService {

    @GET("convert")
    fun getHistoricalData(@Query("q") code: String, @Query("date") dateStart: String, @Query("dateEnd") dateEnd: String): Deferred<Response<Any>>

}