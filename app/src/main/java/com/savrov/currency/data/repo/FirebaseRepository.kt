package com.savrov.currency.data.repo

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class FirebaseRepository(private val storage: FirebaseStorage) {

    fun loadCurriculumVitae(): MutableLiveData<File?> {
        val data = MutableLiveData<File?>()
        val file = File.createTempFile("cv_savrov", "pdf")
        val reference = storage.getReference("doc").child("cv_savrov.pdf")
        reference.getFile(file)
            .addOnSuccessListener {
                data.value = file
            }
            .addOnFailureListener {
                data.value = null
            }
        return data
    }

    fun loadProfilePicture(): MutableLiveData<Uri?> {
        val data = MutableLiveData<Uri?>()
        val reference = storage.getReference("img").child("profile.jpeg")
        reference.downloadUrl
            .addOnSuccessListener {
                data.value = it
            }
            .addOnFailureListener {
                data.value = null
            }
        return data
    }

}