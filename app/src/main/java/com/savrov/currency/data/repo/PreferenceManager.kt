package com.savrov.currency.data.repo

import android.content.SharedPreferences

class PreferenceManager(private val sharedPreferences: SharedPreferences) {

    var baseCurrencyChart: String
        set(value) = sharedPreferences.edit()
            .putString(BASE_CURRENCY_CHART, value)
            .apply()
        get() = sharedPreferences.getString(BASE_CURRENCY_CHART, BASE_CURRENCY_CHART_DEFAULT)?:BASE_CURRENCY_CHART_DEFAULT

    var inputValue: String
        set(value) = sharedPreferences.edit()
            .putString(INPUT_VALUE, value)
            .apply()
        get() = sharedPreferences.getString(INPUT_VALUE, INPUT_VALUE_DEFAULT)?:INPUT_VALUE_DEFAULT

    companion object {
        private const val INPUT_VALUE = "INPUT_VALUE_KEY"
        private const val INPUT_VALUE_DEFAULT = "100"
        private const val BASE_CURRENCY_CHART = "BASE_CURRENCY_CHART_KEY"
        private const val BASE_CURRENCY_CHART_DEFAULT = "EUR"
    }

}