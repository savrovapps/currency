package com.savrov.currency.data.source.remote.model

import com.savrov.currency.data.source.local.model.CurrencyData

data class CurrencyResponse(val isSuccessful: Boolean = false,
                            val date: Long? = null,
                            val data: List<CurrencyData> = arrayListOf(),
                            val error: Error?) {

    data class Error (val code: Int,
                      val text: String)


}