package com.savrov.currency.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.savrov.currency.data.source.local.model.CurrencyData
import com.savrov.currency.data.source.local.model.CurrencyPreference

@Database(entities = [CurrencyData::class, CurrencyPreference::class], version = 4, exportSchema = false)
abstract class Database: RoomDatabase() {

    abstract fun provideCurrencyDao(): CurrencyDao

}