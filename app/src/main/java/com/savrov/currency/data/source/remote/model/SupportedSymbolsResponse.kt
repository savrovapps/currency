package com.savrov.currency.data.source.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SupportedSymbolsResponse(@Expose @SerializedName("success") val success: Boolean,
                                    @Expose @SerializedName("symbols") val symbols: HashMap<String, String>,
                                    @Expose @SerializedName("error") val error: ErrorResponse?)