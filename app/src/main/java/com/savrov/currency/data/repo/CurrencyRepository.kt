package com.savrov.currency.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.savrov.currency.data.source.remote.model.CurrencyResponse
import com.savrov.currency.data.source.local.CurrencyDao
import com.savrov.currency.data.repo.model.CurrencyWithPreference
import com.savrov.currency.data.source.local.model.CurrencyData
import com.savrov.currency.data.source.local.model.CurrencyPreference
import com.savrov.currency.data.source.remote.CurrencyConverterApiService
import com.savrov.currency.data.source.remote.FixerService
import com.savrov.currency.data.source.remote.model.LatesRatesResponse
import com.savrov.currency.data.source.remote.model.SupportedSymbolsResponse
import com.savrov.currency.ui.util.NumberUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CurrencyRepository(private val currencyDao: CurrencyDao, private val fixerService: FixerService, private val currencyConverterApiService: CurrencyConverterApiService) {

    private var currencyDataJobTimestamp: Long? = null
    private val currencyDataJob = CoroutineScope(Dispatchers.IO).launch {
        try {
            val requestRates = fixerService.getLatestRates()
            val requestSymbols = fixerService.getSupportedSymbols()
            val result = compute(requestRates.await(), requestSymbols.await())
            if (result.isSuccessful) {
                currencyDataJobTimestamp = System.currentTimeMillis()
                currencyDao.insertCurrencyDataList(result.data)
                if (currencyDao.loadBaseCurrencyPreference() == null) {
                    val baseCurrencyPreference = CurrencyPreference.valueOf(result.data.sortedBy { it.code }[0])
                    baseCurrencyPreference.isBase = true
                    updateCurrencyPreference(baseCurrencyPreference)
                }
            }
        } catch (exception: Exception) {
            Log.e(TAG, "exception = $exception")
        }
    }

    fun loadCurrencyData(): LiveData<List<CurrencyWithPreference>> {
        val currencyDataJobDeltaInHours = NumberUtil.hoursFrom(currencyDataJobTimestamp?:0)
        if (currencyDataJobDeltaInHours > CURRENCY_DATA_REQUEST_DELTA_IN_HOUR) {
            currencyDataJob.start()
        } else {
            currencyDataJob.cancel()
        }
        return currencyDao.loadCurrencyWithPreferenceList()
    }

    fun loadHistoricalData(currencyCode: String, dateStart: String, dateEnd: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val result = currencyConverterApiService.getHistoricalData("${currencyCode}_EUR", dateStart, dateEnd).await()
                Log.d(TAG, "loadHistoricalData = $result")
                //notifyHistoricalData.postValue(result.body())
            } catch (exception: Exception) {
                Log.e(TAG, "exception = $exception")
            }
        }
    }

    fun updateCurrencyPreference(currencyPreference: CurrencyPreference) {
        CoroutineScope(Dispatchers.IO).launch {
            val preference = currencyDao.loadCurrencyPreference(currencyPreference.currencyCode)
            if (preference == null) {
                currencyDao.insertCurrencyPreference(currencyPreference)
            } else {
                currencyPreference.isBase?.let {
                    val temp = currencyDao.loadBaseCurrencyPreference()
                    // ignore if update "is_base" is not required
                    if (currencyPreference.currencyCode == temp?.currencyCode) return@let
                    if (temp == null) {
                        // prev. base preference was not found
                        currencyDao.insertCurrencyPreference(preference)
                    } else {
                        // update prev & new base preferences
                        temp.isBase = false
                        preference.isBase = true
                        currencyDao.insertCurrencyPreferences(listOf(preference, temp))
                    }
                }
                currencyPreference.isFavorite?.let {
                    preference.isFavorite = currencyPreference.isFavorite
                    currencyDao.insertCurrencyPreference(preference)
                }
            }
        }
    }

    // Is used to combine two responses into one result object
    private fun compute(rates: Response<LatesRatesResponse>, symbols: Response<SupportedSymbolsResponse>): CurrencyResponse {
        val ratesBody = rates.body()
        val symbolsBody = symbols.body()
        if (ratesBody == null || symbolsBody == null) {
            return CurrencyResponse(
                error = CurrencyResponse.Error(
                    0,
                    "Network error. Check internet connection"
                )
            )
        }
        val isSuccessful = ratesBody.success && symbolsBody.success
        val data = mutableListOf<CurrencyData>()
        symbols.body()?.symbols?.forEach {
            data.add(CurrencyData.valueOf(it, rates.body()?.rates?.get(it.key)))
        }
        val date: Long? = rates.body()?.timestamp
        val error: CurrencyResponse.Error? = if (!isSuccessful && ratesBody.error != null) {
            CurrencyResponse.Error(ratesBody.error.code, ratesBody.error.info)
        } else if (!isSuccessful && symbolsBody.error != null) {
            CurrencyResponse.Error(symbolsBody.error.code, symbolsBody.error.info)
        } else {
            null
        }
        return CurrencyResponse(isSuccessful, date, data, error)
    }

    companion object {
        private const val TAG = "TAG_CurrencyRepository"
        private const val CURRENCY_DATA_REQUEST_DELTA_IN_HOUR = 3
    }

}