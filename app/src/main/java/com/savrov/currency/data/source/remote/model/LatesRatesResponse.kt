package com.savrov.currency.data.source.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LatesRatesResponse(@Expose @SerializedName("success") val success: Boolean,
                              @Expose @SerializedName("timestamp") val timestamp: Long,
                              @Expose @SerializedName("base") val base: String,
                              @Expose @SerializedName("date") val date: String,
                              @Expose @SerializedName("rates") val rates: HashMap<String, Double>,
                              @Expose @SerializedName("error") val error: ErrorResponse?)