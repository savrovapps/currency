package com.savrov.currency.data.source.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.savrov.currency.data.repo.model.CurrencyWithPreference
import com.savrov.currency.data.source.local.model.CurrencyData
import com.savrov.currency.data.source.local.model.CurrencyPreference


@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyDataList(data: List<CurrencyData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyPreference(currencyPreference: CurrencyPreference)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrencyPreferences(preference: List<CurrencyPreference>)

    @Query("SELECT currency_data.*, currency_preferences.is_base, currency_preferences.is_favorite FROM currency_data LEFT JOIN currency_preferences ON code = currency_code ORDER BY code, label")
    fun loadCurrencyWithPreferenceList(): LiveData<List<CurrencyWithPreference>>

    @Query("SELECT * FROM currency_preferences WHERE currency_code = :currencyCode")
    fun loadCurrencyPreference(currencyCode: String): CurrencyPreference?

    @Query("SELECT * FROM currency_preferences WHERE is_base = 1")
    fun loadBaseCurrencyPreference(): CurrencyPreference?

}