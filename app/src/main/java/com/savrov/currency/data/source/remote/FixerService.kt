package com.savrov.currency.data.source.remote

import com.savrov.currency.data.source.remote.model.LatesRatesResponse
import com.savrov.currency.data.source.remote.model.SupportedSymbolsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FixerService {

    @GET("symbols")
    fun getSupportedSymbols(): Deferred<Response<SupportedSymbolsResponse>>

    @GET("latest")
    fun getLatestRates(): Deferred<Response<LatesRatesResponse>>

}