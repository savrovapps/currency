package com.savrov.currency.data.source.local.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.savrov.currency.data.source.remote.model.LatesRatesResponse
import com.savrov.currency.data.source.remote.model.SupportedSymbolsResponse

@Entity(tableName = "currency_data")
data class CurrencyData(@PrimaryKey @NonNull @ColumnInfo(name = "code")val code: String,
                        @ColumnInfo(name = "label")val label: String? = null,
                        @ColumnInfo(name = "rate") val rate: Double? = null) {

    companion object {

        fun valueOf(data: Map.Entry<String, String>, rate: Double?): CurrencyData {
            return CurrencyData(code = data.key, label = data.value, rate = rate)
        }

    }

}