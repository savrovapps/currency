package com.savrov.currency.data.source.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorResponse(@Expose @SerializedName("code") val code: Int,
                         @Expose @SerializedName("type") val type: String,
                         @Expose @SerializedName("info") val info: String)