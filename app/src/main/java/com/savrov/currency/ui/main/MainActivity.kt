package com.savrov.currency.ui.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.R
import com.savrov.currency.data.repo.PreferenceManager
import com.savrov.currency.ui.adapter.ViewPagerAdapter
import com.savrov.currency.ui.util.NumberUtil
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var preferenceManager: PreferenceManager

    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        CurrencyApplication.component.inject(this)
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        val bottomSheetBehavior = BottomSheetBehavior.from(activity_main_input_header)
        val currencyListPagerAdapter = ViewPagerAdapter(supportFragmentManager).apply {
            add(MainListFragment.newInstance(false), R.drawable.ic_list)
            add(MainListFragment.newInstance(true), R.drawable.ic_star)
        }
        val bottomPagerAdapter = ViewPagerAdapter(supportFragmentManager).apply {
            add(MainInputFragment.newInstance())
            add(MainChartFragment.newInstance())
        }

        with(activity_main_currency_pager) {
            view_main_currency_tabs.setupWithViewPager(this)
            adapter = currencyListPagerAdapter
            currentItem = 1
        }
        with(view_main_currency_tabs) {
            currencyListPagerAdapter.fragments.forEachIndexed { index, _ ->
                val icon = currencyListPagerAdapter.icons[index] ?: return@with
                getTabAt(index)?.setIcon(icon)
            }
        }

        with(activity_main_input_header) {
            bottomSheetBehavior.setBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(p0: View, p1: Float) {

                }

                override fun onStateChanged(p0: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            with(view_main_input_navigation) {
                                setImageResource(R.drawable.ic_close)
                                setOnClickListener {
                                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                                }
                            }
                        }
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            with(view_main_input_navigation) {
                                setImageResource(R.drawable.ic_menu)
                                setOnClickListener {
                                    MainAboutFragment.newInstance().show(supportFragmentManager, null)
                                }
                            }
                        }
                        else -> {

                        }
                    }
                }
            })
        }
        with(view_main_input_amount) {
            setOnClickListener {
                view_main_bottom_pager.currentItem = 0
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        with(view_main_input_unit) {
            setOnClickListener {
                view_main_bottom_pager.currentItem = 1
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        with(view_main_bottom_pager) {
            adapter = bottomPagerAdapter
        }

        viewModel.notifyCurrencyBase.observe(this, Observer {
            Log.d(TAG, "notifyCurrencyBase = $it")
            with(view_main_input_unit) {
                text = it?.code ?: "N/A"
            }
        })

        viewModel.notifyInput.observe(this, Observer {
            Log.d(TAG, "notifyCurrencyBase = $it")
            preferenceManager.inputValue = it.toString()
            with(view_main_input_amount) {
                text = NumberUtil.formatNumber(it)
            }
        })
    }

    override fun onDestroy() {
        // clear in-app cache folder from "cached" items
        // cached images will be erased too, but fck it in this case.
        applicationContext.cacheDir.deleteRecursively()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "TAG_MainActivity"
    }

}
