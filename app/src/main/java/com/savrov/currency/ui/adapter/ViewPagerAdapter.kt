package com.savrov.currency.ui.adapter

import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {

    val fragments = mutableListOf<Fragment>()
    val icons = mutableListOf<Int?>()

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    fun add(fragment: Fragment, @DrawableRes icon: Int? = null) {
        fragments.add(fragment)
        icons.add(icon)
    }

}