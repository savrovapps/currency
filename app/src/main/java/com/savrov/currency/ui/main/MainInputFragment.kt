package com.savrov.currency.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.R
import com.savrov.currency.data.repo.PreferenceManager
import com.savrov.currency.ui.view.KeyboardView
import kotlinx.android.synthetic.main.fragment_main_input.view.*
import javax.inject.Inject

class MainInputFragment: Fragment() {

    @Inject lateinit var preferenceManager: PreferenceManager

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CurrencyApplication.component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_input, container, false)
        with(view.fragment_main_input_keyboard) {
            addListener(object : KeyboardView.KeyboardViewListener {
                override fun onValueUpdated(value: String) {
                    val temp = if (value.isEmpty()) 0.0 else value.toDouble()
                    viewModel.updateInputValue(temp)
                }
            })
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(requireActivity())[MainViewModel::class.java]
        viewModel.updateInputValue(preferenceManager.inputValue.toDouble())
    }

    companion object {
        private const val TAG = "TAG_MainInputFragment"

        fun newInstance(): MainInputFragment {
            return MainInputFragment()
        }
    }
}