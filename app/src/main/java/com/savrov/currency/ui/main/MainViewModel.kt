package com.savrov.currency.ui.main

import android.net.Uri
import androidx.lifecycle.*
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.data.repo.CurrencyRepository
import com.savrov.currency.data.repo.FirebaseRepository
import com.savrov.currency.data.repo.model.CurrencyWithPreference
import com.savrov.currency.data.source.local.model.CurrencyPreference
import com.savrov.currency.ui.util.NumberUtil
import java.io.File
import javax.inject.Inject

open class MainViewModel: ViewModel() {

    @Inject lateinit var currencyRepository: CurrencyRepository
    @Inject lateinit var firebaseRepository: FirebaseRepository

    val notifyCurrencyWithPreferenceList = MediatorLiveData<List<CurrencyWithPreference>>()
    val notifyCurrencyBase = MediatorLiveData<CurrencyWithPreference?>()
    val notifyChartCurrencyBase = MutableLiveData<CurrencyWithPreference>()
    val notifyInput = MutableLiveData<Double>()
    val notifyError = MediatorLiveData<Pair<Int, String>>()
    val notifyCurriculumVitae = MediatorLiveData<File?>()
    val notifyProfilePicture = MediatorLiveData<Uri?>()

    init {
        CurrencyApplication.component.inject(this)
    }

    fun loadCurrencyList() {
        notifyCurrencyWithPreferenceList.addSource(currencyRepository.loadCurrencyData()) {
            notifyCurrencyWithPreferenceList.value = it
        }
        notifyCurrencyBase.addSource(currencyRepository.loadCurrencyData()) {
            notifyCurrencyBase.value = it.find { it.isBase }
        }
    }

    fun loadCurrencyHistory(currency: CurrencyWithPreference) {
        // TODO
        currencyRepository.loadHistoricalData(currency.code, NumberUtil.formatDate(NumberUtil.dateAddDays(-8)), NumberUtil.formatDate(NumberUtil.dateToday()))
    }

    fun updateCurrencyPreference(preference: CurrencyPreference) {
        currencyRepository.updateCurrencyPreference(preference)
    }

    fun updateInputValue(value: Double) {
        notifyInput.postValue(value)
    }

    fun updateChartCurrencyBase(currencyCode: CurrencyWithPreference) {
        notifyChartCurrencyBase.postValue(currencyCode)
    }

    fun loadCurriculumVitae() {
        notifyCurriculumVitae.addSource(firebaseRepository.loadCurriculumVitae()) {
            notifyCurriculumVitae.value = it
        }
    }

    fun loadProfileUri() {
        notifyProfilePicture.addSource(firebaseRepository.loadProfilePicture()) {
            notifyProfilePicture.value = it
        }
    }
}