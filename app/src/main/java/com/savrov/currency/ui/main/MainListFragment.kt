package com.savrov.currency.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.savrov.currency.R
import com.savrov.currency.data.repo.model.CurrencyWithPreference
import com.savrov.currency.data.source.local.model.CurrencyPreference
import com.savrov.currency.ui.adapter.CurrencyAdapter
import com.savrov.currency.ui.adapter.CurrencyAdapter.ViewHolderType.Companion.CURRENCY_WITH_RATE
import kotlinx.android.synthetic.main.fragment_main_list.view.*

class MainListFragment: Fragment(), CurrencyAdapter.CurrencyAdapterListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var currencyAdapter: CurrencyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currencyAdapter = CurrencyAdapter(CURRENCY_WITH_RATE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_list, container, false)
        with(view.fragment_main_list_currency) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = currencyAdapter
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(requireActivity())[MainViewModel::class.java]
        viewModel.notifyCurrencyWithPreferenceList.observe(this, Observer {
            Log.d(TAG, "notifyCurrencyWithPreferenceList = ${it.size}")
            if (arguments?.getBoolean(FAVORITE_KEY) == true) {
                showFavoriteList(view, it)
            } else {
                showDefaultList(view, it)
            }
        })
        viewModel.notifyInput.observe(this, Observer {
            Log.d(TAG, "notifyInput = $it")
            currencyAdapter.updateValue(it)
        })
        viewModel.notifyCurrencyBase.observe(this, Observer {
            Log.d(TAG, "notifyCurrencyBase = $it")
            currencyAdapter.updateCurrencyBase(it)
        })
    }

    private fun showDefaultList(view: View, data: List<CurrencyWithPreference>?) {
        showDataList(view, data, R.drawable.ic_cloud_off, R.string.fragment_list_default_empty)
    }

    private fun showFavoriteList(view: View, data: List<CurrencyWithPreference>?) {
        showDataList(view, data?.filter { it.isFavorite }, R.drawable.ic_star_border, R.string.fragment_list_favorite_empty)
    }

    private fun showDataList(view: View, data: List<CurrencyWithPreference>?, @DrawableRes errorDrawableRes: Int, @StringRes errorStringRes: Int) {
        with(view.fragment_main_list) {
            displayedChild = if (data.isNullOrEmpty()) 1 else 0
        }
        with(view.fragment_main_list_error_image) {
            setImageResource(errorDrawableRes)
        }
        with(view.fragment_main_list_error_label) {
            text = getString(errorStringRes)
        }
        currencyAdapter.updateData(data?: listOf())
    }

    override fun onItemBaseClicked(currency: CurrencyWithPreference) {
        Log.d(TAG, "onItemBaseClicked = $currency")
        currency.isBase = true
        viewModel.updateCurrencyPreference(CurrencyPreference.valueOf(currency))
    }

    override fun onItemFavoriteClicked(currency: CurrencyWithPreference) {
        Log.d(TAG, "onItemFavoriteClicked = $currency")
        currency.isFavorite = !currency.isFavorite
        viewModel.updateCurrencyPreference(CurrencyPreference.valueOf(currency))
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadCurrencyList()
        currencyAdapter.addListener(this)
    }

    override fun onPause() {
        currencyAdapter.removeListener()
        super.onPause()
    }

    companion object {
        private const val TAG = "TAG_MainListFragment"
        private const val FAVORITE_KEY = "FAVORITE_KEY"

        fun newInstance(showOnlyFavorite: Boolean): MainListFragment {
            return MainListFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(FAVORITE_KEY, showOnlyFavorite)
                }
            }
        }
    }

}