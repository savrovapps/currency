package com.savrov.currency.ui.main

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.R
import com.savrov.currency.data.repo.PreferenceManager
import com.savrov.currency.ui.view.GraphView
import kotlinx.android.synthetic.main.fragment_main_chart.view.*
import javax.inject.Inject

class MainChartFragment: Fragment() {

    @Inject lateinit var preferenceManager: PreferenceManager

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CurrencyApplication.component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_chart, container, false)
        with(view.fragment_main_chart_currency_btn) {
            setOnClickListener {
                view.fragment_main_chart_animator.displayedChild = 1
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(requireActivity())[MainViewModel::class.java]
        viewModel.notifyCurrencyWithPreferenceList.observe(this, Observer { list ->
            if (list.isEmpty()) return@Observer
            with(view.fragment_main_chart_picker) {
                val pickerHandler = Handler()
                val pickerRunnable = Runnable {
                    val currencyBase = list.find { it.isBase }
                    val currencyBaseChart = list[value]
                    Log.d(TAG, "currencyBase=$currencyBase, currencyBaseChart=$currencyBaseChart")
                    view.fragment_main_chart_currency_label.text = getString(R.string.fragment_main_chart_currency_label,
                        currencyBase?.label, currencyBase?.code, currencyBaseChart.label, currencyBaseChart.code)
                    viewModel.updateChartCurrencyBase(currencyBaseChart)
                }
                minValue = 0
                maxValue = if(list.isEmpty()) 1 else list.size-1
                displayedValues = list.map { "${it.label} (${it.code})" }.toTypedArray()
                descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
                setOnScrollListener { _, scrollState ->
                    if (scrollState == NumberPicker.OnScrollListener.SCROLL_STATE_IDLE) {
                        pickerHandler.postDelayed(pickerRunnable, 1250)
                    } else {
                        pickerHandler.removeCallbacks(pickerRunnable)
                    }
                }
                value = list.indexOf(list.find { it.code == preferenceManager.baseCurrencyChart }?:list[0])
                pickerHandler.post(pickerRunnable)
            }
        })
        viewModel.notifyChartCurrencyBase.observe(this, Observer {
            preferenceManager.baseCurrencyChart = it.code
            view.fragment_main_chart_currency_btn.text = it.code
            view.fragment_main_chart_animator.displayedChild = 0
            with(view.fragment_main_chart_view) {
                val data = mutableListOf<GraphView.GraphData>()
                for (i in 0..12) {
                    val value: Float = (2..3).random().toFloat() + ((0..99).random().toFloat()/100)
                    val timestamp = (1514761200000..1546297200000).random()
                    data.add(GraphView.GraphData(value, timestamp))
                }
                updateData(data)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadCurrencyList()
    }

    companion object {
        private const val TAG = "TAG_MainChartFragment"

        fun newInstance(): MainChartFragment {
            return MainChartFragment()
        }
    }

}