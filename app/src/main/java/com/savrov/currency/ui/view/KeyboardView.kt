package com.savrov.currency.ui.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.TableLayout
import android.widget.Toast
import com.savrov.currency.R
import com.savrov.currency.ui.util.NumberUtil
import kotlinx.android.synthetic.main.view_keyboard.view.*

class KeyboardView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : TableLayout(context, attrs), View.OnClickListener {

    companion object {
        private const val TAG = "TAG_KeyboardView"

        const val DOT_SYMBOL = "."
        const val NUMBER_MAX_LENGTH = 10
    }

    var isDotKeyEnabled = true
    var isDelKeyEnabled = true
    var isLeadingZeroRemovable = true

    private val maxFractionDigits = 2

    private var value: StringBuilder = StringBuilder()
    private var listener: KeyboardViewListener? = null

    interface KeyboardViewListener {

        fun onValueUpdated(value: String)

    }

    init {
        inflate(context, R.layout.view_keyboard, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.KeyboardView)
        isDotKeyEnabled = attributes.getBoolean(R.styleable.KeyboardView_is_dot_key_enabled, true)
        isDelKeyEnabled = attributes.getBoolean(R.styleable.KeyboardView_is_del_key_enabled, true)
        isLeadingZeroRemovable = attributes.getBoolean(R.styleable.KeyboardView_is_leading_zero_removable, true)
        attributes.recycle()

        view_keyboard_dot.text = NumberUtil.decimalSeparator.toString()

        view_keyboard_1.setOnClickListener(this)
        view_keyboard_2.setOnClickListener(this)
        view_keyboard_3.setOnClickListener(this)
        view_keyboard_4.setOnClickListener(this)
        view_keyboard_5.setOnClickListener(this)
        view_keyboard_6.setOnClickListener(this)
        view_keyboard_7.setOnClickListener(this)
        view_keyboard_8.setOnClickListener(this)
        view_keyboard_9.setOnClickListener(this)
        view_keyboard_0.setOnClickListener(this)
        view_keyboard_dot.setOnClickListener(this)
        view_keyboard_del.setOnClickListener(this)

        validateLayout()
        validateValue()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.view_keyboard_1 -> addSymbol(1)
            R.id.view_keyboard_2 -> addSymbol(2)
            R.id.view_keyboard_3 -> addSymbol(3)
            R.id.view_keyboard_4 -> addSymbol(4)
            R.id.view_keyboard_5 -> addSymbol(5)
            R.id.view_keyboard_6 -> addSymbol(6)
            R.id.view_keyboard_7 -> addSymbol(7)
            R.id.view_keyboard_8 -> addSymbol(8)
            R.id.view_keyboard_9 -> addSymbol(9)
            R.id.view_keyboard_0 -> addSymbol(0)
            R.id.view_keyboard_dot -> addDot()
            R.id.view_keyboard_del -> removeSymbol()
        }
        validateLayout()
    }

    fun addListener(listener: KeyboardViewListener) {
        this.listener = listener
    }

    fun removeListener() {
        this.listener = null
    }

    private fun validateLayout() {
        view_keyboard_del.isEnabled =
            if (!isDelKeyEnabled) false
            else value.isNotEmpty()
        view_keyboard_dot.isEnabled =
            if (!isDotKeyEnabled) false
            else if (value.isEmpty()) false
            else !value.contains(DOT_SYMBOL)
    }

    private fun validateValue(): String {
        // remove leading zero
        if (isLeadingZeroRemovable && value.length > 1 && value.startsWith("0") && !value.contains(DOT_SYMBOL)) {
            val temp = value.drop(1)
            value.clear()
            value.append(temp)
        }

        //if (value.isEmpty()) value.append(0)
        Log.d(TAG, "value=$value")
        return value.toString().trim()
    }

    private fun removeSymbol() {
        listener?.let {
            if (value.length == 1) {
                value.clear()
            } else {
                val temp = value.dropLast(1)
                value.clear()
                value.append(temp)
            }

            if (value.endsWith(DOT_SYMBOL)) {
                val temp = value.dropLast(1)
                value.clear()
                value.append(temp)
            }

            it.onValueUpdated(validateValue())
        } ?: throw IllegalStateException("KeyboardViewListener is NULL")
    }

    private fun addDot() {
        listener?.let {
            value.append(DOT_SYMBOL)
            it.onValueUpdated(validateValue())
        } ?: throw IllegalStateException("KeyboardViewListener is NULL")
    }

    private fun addSymbol(num: Int) {
        listener?.let {
            if (value.length == NUMBER_MAX_LENGTH) {
                Toast.makeText(context, R.string.view_keyboard_max_length_error, Toast.LENGTH_SHORT).show()
                return
            }
            if (isDotKeyEnabled) {
                if (!view_keyboard_dot.isEnabled && value.isNotEmpty()) {
                    val containsDecimalDigits = value.indexOf(DOT_SYMBOL) != -1
                    if (containsDecimalDigits && value.split(DOT_SYMBOL)[1].count() < maxFractionDigits) {
                        value.append(num)
                    }
                } else {
                    value.append(num)
                }
            } else {
                value.append(num)
            }
            it.onValueUpdated(validateValue())
        } ?: throw IllegalStateException("KeyboardViewListener is NULL")
    }

}