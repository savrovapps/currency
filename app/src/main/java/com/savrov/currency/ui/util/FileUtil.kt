package com.savrov.currency.ui.util

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import java.io.File

object FileUtil {

    fun openPdfFile(context: Context, file: File) {
        val uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(uri, "application/pdf")
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        context.startActivity(intent)
    }

    fun getFileInDownloads(fileName: String): File {
        return getPublicDocumentStorageDir(fileName)
    }

    fun isFileInDownloadsExists(fileName: String): Boolean {
        return getFileInDownloads(fileName).exists()
    }

    fun isExternalStorageWritable(context: Context): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }

    private fun getPublicDocumentStorageDir(fileName: String): File {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        path.mkdirs()
        val file = File(path, fileName)
        return file
    }

}