package com.savrov.currency.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.savrov.currency.data.repo.model.CurrencyWithPreference
import com.savrov.currency.ui.util.NumberUtil
import kotlinx.android.synthetic.main.item_currency_with_rate.view.*
import androidx.annotation.IntDef
import androidx.core.content.ContextCompat
import com.savrov.currency.R
import com.savrov.currency.ui.adapter.CurrencyAdapter.ViewHolderType.Companion.CURRENCY_WITH_RATE
import java.lang.IllegalArgumentException
import com.chauthai.swipereveallayout.ViewBinderHelper

class CurrencyAdapter(@ViewHolderType private val viewHolderType: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    @IntDef(CURRENCY_WITH_RATE)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ViewHolderType {
        companion object {
            const val CURRENCY_WITH_RATE = 0
        }
    }

    private val viewBinderHelper = ViewBinderHelper()
    private val data = mutableListOf<CurrencyWithPreference>()
    private var baseCurrency: CurrencyWithPreference? = null
    private var inputValue: Double = 1.0
    private var listener: CurrencyAdapterListener? = null

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType) {
            CURRENCY_WITH_RATE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_currency_with_rate, parent, false)
                return CurrencyWithRateViewHolder(view, listener)
            }
            else -> throw IllegalArgumentException("Unknown viewType: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            CURRENCY_WITH_RATE -> {
                (holder as CurrencyWithRateViewHolder).apply {
                    bind(data[position], baseCurrency, inputValue)
                    viewBinderHelper.bind(holder.itemView.item_currency_with_rate, data[position].code)
                }
            }
            else -> throw IllegalArgumentException("Unknown viewType: ${holder.itemViewType}")
        }
    }

    override fun getItemCount() = data.size

    fun updateData(data: List<CurrencyWithPreference>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun updateCurrencyBase(baseCurrency: CurrencyWithPreference?) {
        if (baseCurrency == null) return
        this.baseCurrency = baseCurrency
        notifyDataSetChanged()
    }

    fun updateValue(inputValue: Double) {
        this.inputValue = inputValue
        notifyDataSetChanged()
    }

    fun addListener(listener: CurrencyAdapterListener) {
        this.listener = listener
    }

    fun removeListener() {
        this.listener = null
    }

    override fun getItemViewType(position: Int): Int {
        return viewHolderType
    }

    interface CurrencyAdapterListener {

        fun onItemBaseClicked(currency: CurrencyWithPreference)

        fun onItemFavoriteClicked(currency: CurrencyWithPreference)

    }

    class CurrencyWithRateViewHolder(private val view: View, private val listener: CurrencyAdapterListener?) : RecyclerView.ViewHolder(view) {

        fun bind(currency: CurrencyWithPreference, baseCurrency: CurrencyWithPreference?, inputValue: Double) {
            val rate = NumberUtil.convertCurrencyRate(baseCurrency?.rate, currency.rate)
            with(view.item_currency_with_rate_foreground) {
                val color = ContextCompat.getColor(view.context, if (currency.code == baseCurrency?.code) {
                    R.color.colorPrimaryDark
                } else {
                    R.color.colorPrimary
                })
                setBackgroundColor(color)
                setOnClickListener {
                    listener?.onItemBaseClicked(currency)
                }
            }
            with(view.item_currency_with_rate_name) {
                text = currency.label
            }
            with(view.item_currency_with_rate_value) {
                text = if (rate == Double.NaN) {
                    "N/A"
                } else {
                    NumberUtil.formatCurrency(rate * inputValue, currency.code)
                }
            }
            with(view.item_currency_with_rate_rate) {
                text = if (rate == Double.NaN) {
                    "N/A"
                } else {
                    NumberUtil.formatNumber(rate, 5)
                }
            }
            with(view.item_currency_with_rate_symbol) {
                text = currency.code
            }
            with(view.item_currency_with_rate_favorite) {
                setImageResource(if (currency.isFavorite) {
                    R.drawable.ic_star
                } else {
                    R.drawable.ic_star_border
                })
                setOnClickListener {
                    listener?.onItemFavoriteClicked(currency)
                }
            }
        }

    }

}