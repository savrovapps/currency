package com.savrov.currency.ui.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.savrov.currency.R
import com.savrov.currency.ui.util.FileUtil.getFileInDownloads
import com.savrov.currency.ui.util.FileUtil.isExternalStorageReadable
import com.savrov.currency.ui.util.FileUtil.isExternalStorageWritable
import com.savrov.currency.ui.util.FileUtil.isFileInDownloadsExists
import com.savrov.currency.ui.util.FileUtil.openPdfFile
import kotlinx.android.synthetic.main.fragment_main_about.view.*

class MainAboutFragment: BottomSheetDialogFragment() {

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_about, container, false)
        with(view.fragment_main_about_bitbucket) {
            setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.bitbucket_link)))
                startActivity(browserIntent)
            }
        }
        with(view.fragment_main_about_linkedin) {
            setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.linkedin_link)))
                startActivity(browserIntent)
            }
        }
        with(view.fragment_main_about_cv) {
            setOnClickListener {
                if (isExternalStorageReadable() && isFileInDownloadsExists(FILE_CV)) {
                    openPdfFile(requireContext(), getFileInDownloads(FILE_CV))
                } else {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        viewModel.loadCurriculumVitae()
                    } else {
                        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_EXTERNAL_STORAGE_CODE)
                    }
                }
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(requireActivity())[MainViewModel::class.java]
        viewModel.notifyCurriculumVitae.observe(this, Observer {
            if (it == null) {
                Toast.makeText(context, getString(com.savrov.currency.R.string.file_download_error), Toast.LENGTH_LONG).show()
            } else {
                 if (isExternalStorageWritable(requireContext())) {
                     val file = getFileInDownloads(FILE_CV)
                     it.copyTo(file, true)
                     MediaScannerConnection.scanFile(context, arrayOf(file.toString()), null) { path, _ ->
                         Log.d(TAG, "onScanCompleted:: path=$path")
                     }
                     openPdfFile(requireContext(), getFileInDownloads(FILE_CV))
                 } else {
                     openPdfFile(requireContext(), it)
                 }
            }
        })
        viewModel.notifyProfilePicture.observe(this, Observer {
            val requestOptions = RequestOptions().apply {
                transform(CenterCrop(), RoundedCorners(14))
            }
            Glide.with(this)
                .load(it)
                .apply(requestOptions)
                .into(view.fragment_main_about_photo)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadProfileUri()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            WRITE_EXTERNAL_STORAGE_CODE -> {
                viewModel.loadCurriculumVitae()
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, getString(R.string.file_write_permission_denied), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    companion object {
        private const val TAG = "TAG_MainAboutFragment"
        private const val FILE_CV = "cv_savrov.pdf"
        private const val WRITE_EXTERNAL_STORAGE_CODE = 455

        fun newInstance(): MainAboutFragment {
            return MainAboutFragment()
        }
    }
}