package com.savrov.currency.ui.util

import android.content.res.Resources
import android.util.TypedValue
import java.text.DateFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object NumberUtil {

    val decimalSeparator: Char
        get() {
            val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
            val temp = numberFormat.format(0.0)
            return temp[1]
        }

    fun convertCurrencyRate(baseRate: Double?, targetRate: Double?): Double {
        if (baseRate == null || targetRate == null) return Double.NaN
        return targetRate / baseRate
    }

    fun formatNumber(value: Double?, fractionDigits: Int = 2): String {
        if (value == null) return "N/A"
        val format = NumberFormat.getNumberInstance(Locale.getDefault()).apply {
            minimumFractionDigits = fractionDigits
            maximumFractionDigits = fractionDigits
        }
        return format.format(value)
    }

    fun formatCurrency(amount: Double?, code: String?): String {
        if (amount == null || code == null) return "N/A"
        val format = NumberFormat.getCurrencyInstance(Locale.getDefault()).apply {
            minimumFractionDigits = 2
            maximumFractionDigits = 2
        }
        return format.format(amount)
            .replace(format.currency.symbol, "", true)
            .trim()
    }

    fun hoursFrom(timestampInPast: Long): Long {
        val deltaInMillis = System.currentTimeMillis() - timestampInPast
        return TimeUnit.MILLISECONDS.toHours(deltaInMillis)
    }

    fun dpToPx(resources: Resources, value: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics);
    }

    fun dateToday(): Calendar {
        return Calendar.getInstance()
    }

    fun dateAddDays(days: Int): Calendar {
        val calendar = dateToday()
        calendar.add(Calendar.DAY_OF_YEAR, days)
        return calendar
    }

    fun formatDate(calendar: Calendar, pattern: String = "yyyy-MM-dd"): String {
        try {
            val dateFormat = SimpleDateFormat(pattern)
            return dateFormat.format(calendar.time)
        } catch (ex: ParseException) {
            ex.printStackTrace();
            return ""
        }
    }

}