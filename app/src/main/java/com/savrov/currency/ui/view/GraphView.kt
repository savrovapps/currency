package com.savrov.currency.ui.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.savrov.currency.R
import com.savrov.currency.ui.util.NumberUtil


class GraphView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    companion object {
        private const val TAG = "TAG_GraphView"
        private const val PAINT_WIDTH_DP = 2.5f
        private const val POINT_DOT_RADIUS_DP = 3f
        private const val PAINT_LEVEL_LINE_DP = 0.75f
        private const val PAINT_075_DP = 0.75f
    }

    data class GraphData(val value: Float, val timestamp: Long)

    private val paintLine = Paint()
    private val paintLinePoint = Paint()
    private val paintLineThick = Paint()
    private val paintLevelLine = Paint()
    private val paintTextLabel = Paint()

    private val path = Path()

    private val data = mutableListOf<GraphData>()

    init {
        with(paintLine) {
            color = ContextCompat.getColor(context, android.R.color.white)
            strokeWidth = NumberUtil.dpToPx(context.resources, PAINT_WIDTH_DP)
            style = Paint.Style.STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
        }

        with(paintLinePoint) {
            color = ContextCompat.getColor(context, android.R.color.white)
            strokeWidth = NumberUtil.dpToPx(context.resources, POINT_DOT_RADIUS_DP)
            style = Paint.Style.FILL_AND_STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
        }

        with(paintLineThick) {
            color = ContextCompat.getColor(context, android.R.color.white)
            strokeWidth = NumberUtil.dpToPx(context.resources, PAINT_075_DP)
            alpha = 120
            style = Paint.Style.STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
        }

        with(paintLevelLine) {
            color = ContextCompat.getColor(context, R.color.colorYellow)
            strokeWidth = NumberUtil.dpToPx(context.resources, PAINT_LEVEL_LINE_DP)
            style = Paint.Style.FILL_AND_STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
        }

        with(paintTextLabel) {
            style = Paint.Style.FILL
            isAntiAlias = true
            color = Color.WHITE
            isFakeBoldText = true
            //textAlign = Paint.Align.RIGHT
            textSize = 32f
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawAxes(canvas)
        drawLineGraph(canvas)
        drawPoints(canvas)
        //drawLevelLine(canvas)
        super.onDraw(canvas)
    }

    private fun drawAxes(canvas: Canvas) {

        val stepVertical = 1

        val dataMax = data.maxBy { it.value }?.value?:0f
        val dataMaxCeil = dataMax + (stepVertical - dataMax % stepVertical)

        val dataMin = data.minBy { it.value }?.value?:0f
        val dataMinCeil = dataMin - (dataMin % stepVertical)

        for (i in dataMinCeil.toInt()..dataMaxCeil.toInt() step stepVertical) {
            val lineY = getYFromValue(i.toFloat(), dataMaxCeil, dataMinCeil)
            val textY = lineY + (paintTextLabel.textSize /3)
            canvas.drawText(i.toString(), paddingStart.toFloat(), textY, paintTextLabel)
            canvas.drawLine(NumberUtil.dpToPx(context.resources, 40f), lineY , width.toFloat() - paddingEnd, lineY, paintLineThick)
        }
    }

    private fun drawLineGraph(canvas: Canvas) {
        if (data.isEmpty()) return
        path.reset()

        val pointsPre = mutableListOf<PointF>()
        val points = mutableListOf<PointF>()
        val pointsPost = mutableListOf<PointF>()
        val max = data.maxBy { it.value }?.value?:0f
        val min = data.minBy { it.value }?.value?:0f

        for (i in 0 until data.size) {
            pointsPre.add(PointF())
            points.add(PointF())
            pointsPost.add(PointF())
        }

        for (i in 0 until data.size) {
            points[i].set(getXFromIndex(i, data.size) + NumberUtil.dpToPx(context.resources, 40f), getYFromValue(data[i].value, max, min))
        }

        for (i in 1 until data.size) {
            pointsPre[i].set((points[i].x + points[i-1].x) / 2, points[i-1].y)
            pointsPost[i].set((points[i].x + points[i-1].x) / 2, points[i].y)
        }

        path.rMoveTo(points[0].x, points[0].y)
        for (i in 1 until data.size) {
            path.cubicTo(pointsPre[i].x, pointsPre[i].y, pointsPost[i].x, pointsPost[i].y, points[i].x, points[i].y)
        }

        canvas.drawPath(path, paintLine)
    }

    private fun drawPoints(canvas: Canvas) {
        val max = data.maxBy { it.value }?.value?:0f
        val min = data.minBy { it.value }?.value?:0f

        for (i in 0 until data.size) {
            val point = PointF(getXFromIndex(i, data.size) + NumberUtil.dpToPx(context.resources, 40f), getYFromValue(data[i].value, max, min))
            canvas.drawCircle(point.x, point.y, NumberUtil.dpToPx(context.resources, POINT_DOT_RADIUS_DP), paintLinePoint)
        }

    }

    private fun drawLevelLine(canvas: Canvas) {
        val temp = data.last()
        val max = data.maxBy { it.value }?.value?:0f
        val min = data.minBy { it.value }?.value?:0f
        canvas.drawLine(0f, getYFromValue(temp.value, max, min), getXFromIndex(data.size, data.size), getYFromValue(temp.value, max, min), paintLevelLine)
    }

    fun updateData(data: MutableList<GraphData>) {
        val temp = mutableListOf<GraphData>()
        temp.addAll(data.sortedBy { it.timestamp })
        Log.d(TAG, "data = ${data}")

        this.data.clear()
        this.data.addAll(temp)
        invalidate()
    }

    private fun getYFromValue(value: Float, max: Float, min: Float): Float {
        val height = getHeight() - getPaddingTop() - getPaddingBottom();
        return ((max - value) * (height) / (max - min)) + paddingTop
    }

    private fun getXFromIndex(index: Int, max: Int): Float {
        return index.toFloat() * ((width - NumberUtil.dpToPx(context.resources, 40f) - paddingStart)/ (max.toFloat() - 1.0f))
    }

}