package com.savrov.currency.di.module

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.data.repo.CurrencyRepository
import com.savrov.currency.data.source.local.Database
import com.savrov.currency.data.source.remote.CurrencyConverterApiService
import com.savrov.currency.data.source.remote.FixerService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RepositoryModule() {

    companion object {
        private val FIXER_TOKEN_LIST = arrayOf("22ff3aeb3d234f2757c44f16b3894868",
            "6a2736710f5a573c6d2f0c58f1a48fe7", "406b850345d018eea1b77ebb6b90cc70", "dd83eedf4d3252efc29950acdaafd2da",
            "6ed074bd7f2e8c1da0d48c2d1c24d3fe", "2b651ef5fd8758bb2d24f6f14ce750d5", "fad692a1bf21d67e3b31126eed29890c",
            "7b6ee0444d4ba2310a6e8f534acdf6b9", "ac473d6594baa9cef012c91979eb4862", "56f4010f0143bc903fc8ddffe9a178b3")
        private val CURRENCY_CONVERTER_API_TOKEN_LIST = arrayOf("287caa6214f6b5eec5d0")
    }

    @Singleton
    @Provides
    fun provideDatabase(application: CurrencyApplication): Database {
        return Room.databaseBuilder(application, Database::class.java, "currency_db").fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideFixerService(): FixerService {
        return Retrofit.Builder()
            .baseUrl("http://data.fixer.io/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(OkHttpClient.Builder()
                .addInterceptor {
                    val url = it.request().url().newBuilder()
                        .addQueryParameter("access_key", FIXER_TOKEN_LIST.random())
                        .build()
                    return@addInterceptor it.proceed(it.request().newBuilder().url(url).build())
                }
                .build())
            .build()
            .create(FixerService::class.java)
    }

    @Singleton
    @Provides
    fun provideCurrencyConverterApiService(): CurrencyConverterApiService {
        return Retrofit.Builder()
            .baseUrl("https://free.currconv.com/api/v7/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(OkHttpClient.Builder()
                .addInterceptor {
                    val url = it.request().url().newBuilder()
                        .addQueryParameter("apiKey", CURRENCY_CONVERTER_API_TOKEN_LIST.random())
                        .addQueryParameter("compact", "ultra")
                        .build()
                    return@addInterceptor it.proceed(it.request().newBuilder().url(url).build())
                }
                .build())
            .build()
            .create(CurrencyConverterApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideFirebaseDatabse(): FirebaseDatabase = FirebaseDatabase.getInstance();

    @Singleton
    @Provides
    fun provideFirebaseStorage() = FirebaseStorage.getInstance()

    @Singleton
    @Provides
    fun provideSharedPreference(application: CurrencyApplication): SharedPreferences {
        return application.getSharedPreferences("currency.shp", Context.MODE_PRIVATE)
    }

}