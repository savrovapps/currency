package com.savrov.currency.di.module

import android.content.Context
import android.content.SharedPreferences
import com.google.android.gms.common.util.SharedPreferencesUtils
import com.google.firebase.storage.FirebaseStorage
import com.savrov.currency.CurrencyApplication
import com.savrov.currency.data.repo.CurrencyRepository
import com.savrov.currency.data.repo.FirebaseRepository
import com.savrov.currency.data.repo.PreferenceManager
import com.savrov.currency.data.source.local.Database
import com.savrov.currency.data.source.remote.CurrencyConverterApiService
import com.savrov.currency.data.source.remote.FixerService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: CurrencyApplication) {

    @Provides
    @Singleton
    fun provideApplication(): CurrencyApplication {
        return application
    }

    @Singleton
    @Provides
    fun provideCurrencyRepository(database: Database, fixerService: FixerService, currencyConverterApiService: CurrencyConverterApiService): CurrencyRepository {
        return CurrencyRepository(database.provideCurrencyDao(), fixerService, currencyConverterApiService)
    }

    @Singleton
    @Provides
    fun provideFirebaseRepository(storage: FirebaseStorage): FirebaseRepository {
        return FirebaseRepository(storage)
    }

    @Singleton
    @Provides
    fun providePreferenceManager(preferences: SharedPreferences): PreferenceManager {
        return PreferenceManager(preferences)
    }

}