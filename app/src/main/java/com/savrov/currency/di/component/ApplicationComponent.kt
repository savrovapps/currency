package com.savrov.currency.di.component

import android.content.SharedPreferences
import com.savrov.currency.data.repo.CurrencyRepository
import com.savrov.currency.data.repo.FirebaseRepository
import com.savrov.currency.di.module.ApplicationModule
import com.savrov.currency.di.module.RepositoryModule
import com.savrov.currency.ui.main.MainActivity
import com.savrov.currency.ui.main.MainChartFragment
import com.savrov.currency.ui.main.MainInputFragment
import com.savrov.currency.ui.main.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class])
interface ApplicationComponent {

    fun inject(view: MainActivity)

    fun inject(view: MainInputFragment)

    fun inject(view: MainChartFragment)

    fun inject(viewModel: MainViewModel)

    fun currencyRepository(): CurrencyRepository

    fun firebaseRepository(): FirebaseRepository

    fun sharedPreference(): SharedPreferences

}