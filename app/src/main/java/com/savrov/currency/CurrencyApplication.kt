package com.savrov.currency

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.savrov.currency.di.component.ApplicationComponent
import com.savrov.currency.di.component.DaggerApplicationComponent
import com.savrov.currency.di.module.ApplicationModule
import com.savrov.currency.di.module.RepositoryModule

class CurrencyApplication: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .repositoryModule(RepositoryModule())
            .build()
    }

    companion object {
        lateinit var component: ApplicationComponent
    }

}